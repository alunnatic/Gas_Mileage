package al.gasmileage;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;

public class Camera extends AppCompatActivity {

    double miles;
    double gallons;
    SurfaceView camView;
    TextView bottomOfPage;
    CameraSource cameraSource;
    private static final String TAG = "MainActivity";
    final int RequestCameraPermissionID = 1001;
    ParseResult sendToParse = new ParseResult();
    double showResultCheckVar;
    boolean gallonFlag = false;
    boolean milesFlag = false;
    //String showResult;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode){
            case RequestCameraPermissionID:
            {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    {
                        return;
                    }
                    try {
                        cameraSource.start(camView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        camView = findViewById(R.id.surfaceView);
        bottomOfPage = findViewById(R.id.bottom_text);

        Context context = getApplicationContext();

        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();
        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Detector dependencies are not yet available.");
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        } else {
            cameraSource =
                    new CameraSource.Builder(getApplicationContext(), textRecognizer)
                            .setFacing(CameraSource.CAMERA_FACING_BACK)
                            .setRequestedPreviewSize(1280, 1024)
                            .setRequestedFps(2.0f)
                            .build();
            camView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(Camera.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    RequestCameraPermissionID);
                            return;
                        }
                        cameraSource.start(camView.getHolder());
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {

                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if(items.size() != 0){
                        bottomOfPage.post(new Runnable() {
                            @Override
                            public void run() {
                                final StringBuilder stringBuilder = new StringBuilder();
                                //build the string that the OCR is seeing
                                for(int i = 0; i < items.size(); i++){
                                    TextBlock item = items.valueAt(i);
                                    stringBuilder.append(item.getValue());
                                }

                                //This is running the string built by ocr through the ParseResult class to display
                                //the parsed result only to the user
                                showResultCheckVar = sendToParse.MakeItNumbersOnly("miles", stringBuilder);
                                if(showResultCheckVar == -1){
                                    bottomOfPage.setText("Number Not Found");
                                }else {
                                    bottomOfPage.setText(Double.toString(showResultCheckVar));
                                }
                                //bottomOfPage.setText(stringBuilder);


                                //Miles button logic to send the currently displayed string to the ParseResult class for parsing
                                Button milesButton = findViewById(R.id.mi);
                                milesButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //parse the result through the ParseResult class
                                        miles = showResultCheckVar;//sendToParse.MakeItNumbersOnly("miles", stringBuilder);

                                        //show the result in the result activity
                                        Intent snapPic = new Intent(getApplicationContext(), Result.class);
                                        snapPic.putExtra("displayedResult", Double.toString(showResultCheckVar));
                                        snapPic.putExtra("whichVar", "miles");
                                        //snapPic.putExtra("displayedResult", Double.toString(miles));
                                        startActivityForResult(snapPic, 1);
                                    }
                                });

                                //Gallons button logic to send the currently displayed string to the ParseResult class for parsing
                                Button gallonsButton = findViewById(R.id.gal);
                                gallonsButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //parse the result through the ParseResult class
                                        gallons = showResultCheckVar;//sendToParse.MakeItNumbersOnly("gallons", stringBuilder);//bottomOfPage.getText().toString());

                                        //show the result in the result activity
                                        Intent snapPic = new Intent(getApplicationContext(), Result.class);
                                        snapPic.putExtra("displayedResult", Double.toString(showResultCheckVar));
                                        snapPic.putExtra("whichVar", "gallons");
                                        //snapPic.putExtra("displayedResult", Double.toString(showResultCheckVar));
                                        startActivityForResult(snapPic, 1);
                                    }
                                });

                            }
                        });
                    }
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
                String who = data.getStringExtra("who");
                if(result.equals("yes")){
                    if(who.equals("miles")){ milesFlag = true; }
                    if(who.equals("gallons")){ gallonFlag = true; }
                }
                if(result.equals("no")){
                    if(who.equals("miles")){
                        miles = 0;
                        milesFlag = false;
                    }
                    if(who.equals("gallons")){
                        gallons = 0;
                        gallonFlag = false;
                    }
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
