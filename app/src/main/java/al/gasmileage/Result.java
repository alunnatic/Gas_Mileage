package al.gasmileage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Result extends AppCompatActivity {

    TextView resultFromCam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        resultFromCam = findViewById(R.id.resultFromCamera);

        String resultFromCameraClass = getIntent().getStringExtra("displayedResult");
        final String whoCalled = getIntent().getStringExtra("whichVar");
        resultFromCam.setText(resultFromCameraClass);

        Button yesButton = findViewById(R.id.buttonYes);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "yes");
                returnIntent.putExtra("who", whoCalled);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

        Button noButton = findViewById(R.id.buttonNo);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "no");
                returnIntent.putExtra("who", whoCalled);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

    }

}
