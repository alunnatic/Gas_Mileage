package al.gasmileage;

import org.apache.commons.lang3.math.NumberUtils;

public class ParseResult {

    double miles = 0;
    double gallons = 0;
    double noMilesOrGallonsFound = 0;

    public double MakeItNumbersOnly(String milesOrGallons, StringBuilder stringB){
        //looking to parse gallons
        if(milesOrGallons.equals("gallons")){
            gallons = parseMethod(stringB);
            return gallons;
        }

        //looking to parse miles
        else if(milesOrGallons.equals("miles")){
            miles = parseMethod(stringB);
            return miles;
        }else return noMilesOrGallonsFound;
    }

    private double parseMethod(StringBuilder strBuild){
        double parsedDoubleToReturn;
        int matchCounter = 1;
        //convert to string for use of String methods so all non letter/numbers and periods are
        // removed, and all letter "o" are converted to number "0".
        //then converted back to stringbuilder type for access to stringbuilder methods
        String tempString = strBuild.toString();
        tempString = tempString.replaceAll("[^A-Za-z0-9.]", "");
        tempString = tempString.replace("o", "0");
        StringBuilder sB = new StringBuilder(tempString);

        //main loop for parsing
        //the "for" loops are incremented at the end and "continue" is used instead of decrementing
        //because otherwise there are indexOutOfBounds type errors with the loops, but only when
        //this method is called for the display at the bottom of the screen. Not when called from
        //a button press, not sure why only in that instance
        for (int i = 0; i < sB.length(); ){
            if(sB.charAt(0) == '.'){
                sB.deleteCharAt(0);
                continue;
            }
            //if it is a letter get rid of it
            if(Character.isLetter(sB.charAt(i))){
                sB.deleteCharAt(i);
                continue;
            }
            //if it is a number use a counter to see if this number is followed by 5 more numbers
            //because miles on odometer read xxxxxx
            else if(Character.isDigit(sB.charAt(i))){

                for(int n = 1; n < 6; ){
                    sB.trimToSize();
                    if(sB.length() > n) {
                        if ((Character.isDigit(sB.charAt(n)) || (sB.charAt(n) == '.'))) {
                            matchCounter++;
                        }
                        else {
                            matchCounter = 1;
                            break;
                        }
                        n++;
                    }else {
                        matchCounter = 1;
                        break;
                    }
                }
                //if you don't find 5 more numbers (or 4 numbers and a period), delete it
                if((matchCounter < 6) && !(sB.charAt(i) == '.')){
                    sB.deleteCharAt(i);
                    continue;
                }
                //you have found 6 numbers, or 5 numbers and a period
                //delete everything after the last number and break out of the loop
                else if(matchCounter >= 6){
                    if(sB.length() == 6){
                        break;
                    }
                    else {
                        sB.trimToSize();
                        sB.delete(6, sB.length());
                    }
                    break;
                }
            }
            i++;
        }
        //a default number for testing when a bad number comes through
        double returnThisAsABadAnswer = -1;
        boolean isThisAGoodNumber = false;
        //make sure the string is in fact a string 6 characters long, and they are numbers that
        //will convert to a double
        if(sB.length() == 6){
            isThisAGoodNumber = NumberUtils.isParsable(sB.toString());
        }
        //if the number checks out cast the string to double or else return the bad answer
        if(isThisAGoodNumber){
            parsedDoubleToReturn = Double.parseDouble(sB.toString());
        }else{ parsedDoubleToReturn = returnThisAsABadAnswer; }

        return parsedDoubleToReturn;
    }
}
